const AWS = require('aws-sdk');
const axios = require('axios');
const sharp = require('sharp');

const { STAGE, CACHE_BUCKET, ORIG_URL } = process.env;

const getS3Client = () => {
  if (STAGE === 'local') {
    return new AWS.S3({
      s3ForcePathStyle: true,
      accessKeyId: 'S3RVER',
      secretAccessKey: 'S3RVER',
      endpoint: new AWS.Endpoint('http://localhost:8000'),
    });
  }

  return new AWS.S3();
};

const downloadImage = async (url) => {
  const response = await axios.get(url, {
    responseType: 'arraybuffer',
  });

  const buffer = response.data;
  return {
    buffer,
    contentType: response.headers['content-type'],
  };
};

const resizeImage = (origBuffer, width, height) => sharp(origBuffer).resize(parseInt(width), parseInt(height), {
  fit: sharp.fit.inside,
  withoutEnlargement: true,
}).toBuffer();

const buildImageResponse = (body, contentType, key, isCache) => ({
  isBase64Encoded: true,
  statusCode: 200,
  body: Buffer.from(body, 'binary').toString('base64'),
  headers: {
    'Content-Type': contentType,
    'Content-Disposition': `attachment; filename="${key}"`,
    'X-Cache-Hit': isCache ? '1' : '0',
  },
});

module.exports = {
  getS3Client,
  downloadImage,
  resizeImage,
  buildImageResponse,
};
