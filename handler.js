const { getS3Client, resizeImage } = require('./utils');
const { downloadImage, buildImageResponse } = require('./utils.js');

const { ORIG_URL, CACHE_BUCKET } = process.env;

module.exports.resizer = async (event) => {
  const { pathParameters: { path } } = event;
  console.log(path);


  // Get params from path (key, width & height)
  const [, width, height, key] = path.match(new RegExp(/\/?(\d+)\/(\d+)\/(.*)/));
  const cacheBucketKey = `${width}x${height}/${key}`;
  console.log(`Resizing ${key} to max ${width}x${height}`);

  // Serve from cache if it exists
  const s3 = getS3Client();
  try {
    const cacheObject = await s3.getObject({ Key: cacheBucketKey, Bucket: CACHE_BUCKET }).promise();
    console.log(`Cache Hit ${cacheBucketKey}`)

    return buildImageResponse(cacheObject.Body, cacheObject.ContentType, key, true);
  } catch (e) {
    console.log('Cache Miss, trying original');
  }


  // Download from unislim server
  const { buffer: original, contentType } = await downloadImage(`${ORIG_URL}/${key}`);


  // Resize
  const resized = await resizeImage(original, width, height);


  // Store to cache
  await s3.putObject({
    Key: cacheBucketKey,
    Bucket: CACHE_BUCKET,
    Body: resized,
    ContentType: contentType,
    ContentDisposition: `attachment; filename="${key}"`,
  }).promise();


  // Serve to client
  return buildImageResponse(resized, contentType, key);
};
